# Translation of docs_krita_org_user_manual___snapping.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-17 18:33+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../user_manual/snapping.rst:1
msgid "How to use the snapping functionality in Krita."
msgstr "Com utilitzar la funcionalitat d'ajust en el Krita."

#: ../../user_manual/snapping.rst:10 ../../user_manual/snapping.rst:43
msgid "Guides"
msgstr "Guies"

#: ../../user_manual/snapping.rst:10
msgid "Snap"
msgstr "Ajustar"

#: ../../user_manual/snapping.rst:10
msgid "Vector"
msgstr "Vectors"

#: ../../user_manual/snapping.rst:15
msgid "Snapping"
msgstr "Ajustar"

#: ../../user_manual/snapping.rst:17
msgid ""
"In Krita 3.0, we now have functionality for Grids and Guides, but of course, "
"this functionality is by itself not that interesting without snapping."
msgstr ""
"En el Krita 3.0, ara tenim la funcionalitat per a Quadrícules i Guies, però, "
"és clar, aquesta funcionalitat no és tan interessant sense poder ajustar."

#: ../../user_manual/snapping.rst:21
msgid ""
"Snapping is the ability to have Krita automatically align a selection or "
"shape to the grids and guides, document center and document edges. For "
"Vector layers, this goes even a step further, and we can let you snap to "
"bounding boxes, intersections, extrapolated lines and more."
msgstr ""
"L'ajustament és la capacitat que té el Krita d'alinear automàticament una "
"selecció o forma amb les quadrícules i guies, el centre del document i les "
"vores del document. Per a les capes vectorials, això va fins i tot un pas "
"més enllà, i podem permetre-li que s'ajusti als contenidors, interseccions, "
"línies extrapolades i més."

#: ../../user_manual/snapping.rst:26
msgid ""
"All of these can be toggled using the snap pop-up menu which is assigned to :"
"kbd:`Shift + S` shortcut."
msgstr ""
"Tots aquests es poden alternar utilitzant el menú emergent d'ajustar, el "
"qual està assignat a la drecera :kbd:`Majús. + S`."

#: ../../user_manual/snapping.rst:29
msgid "Now, let us go over what each option means:"
msgstr "Ara, repassarem què vol dir cada opció:"

#: ../../user_manual/snapping.rst:32
msgid ""
"This will snap the cursor to the current grid, as configured in the grid "
"docker. This doesn’t need the grid to be visible. Grids are saved per "
"document, making this useful for aligning your art work to grids, as is the "
"case for game sprites and grid-based designs."
msgstr ""
"Ajustarà el cursor a la quadrícula actual, segons la configuració a "
"l'acoblador Quadrícula. No es necessita que la quadrícula sigui visible. Les "
"quadrícules es desen per document, el qual resulta útil per alinear el "
"vostre treball artístic amb les quadrícules, com és el cas dels dissenys amb "
"franges i els basats en quadrícules per als jocs."

#: ../../user_manual/snapping.rst:34
msgid "Grids"
msgstr "Quadrícules"

#: ../../user_manual/snapping.rst:37
msgid "Pixel"
msgstr "Píxel"

#: ../../user_manual/snapping.rst:37
msgid ""
"This allows to snap to every pixel under the cursor. Similar to Grid "
"Snapping but with a grid having spacing = 1px and offset = 0px."
msgstr ""
"Permet ajustar a cada píxel sota el cursor. Similar a Ajusta a la quadrícula "
"però amb una quadrícula amb Espaiat = 1 px i Desplaçament = 0 px."

#: ../../user_manual/snapping.rst:40
msgid ""
"This allows you to snap to guides, which can be dragged out from the ruler. "
"Guides do not need to be visible for this, and are saved per document. This "
"is useful for comic panels and similar print-layouts, though we recommend "
"Scribus for more intensive work."
msgstr ""
"Permet ajustar a les guies, les quals es poden arrossegar des de la regla. "
"No es necessita que les guies siguin visibles i es desen per document. Això "
"és útil per a vinyetes i disposicions d'impressió similars per a còmics, "
"encara que recomanem Scribus per a un treball més intensiu."

#: ../../user_manual/snapping.rst:46
msgid ".. image:: images/snapping/Snap-orthogonal.png"
msgstr ".. image:: images/snapping/Snap-orthogonal.png"

#: ../../user_manual/snapping.rst:48
msgid ""
"This allows you to snap to a horizontal or vertical line from existing "
"vector objects’s nodes (Unless dealing with resizing the height or width "
"only, in which case you can drag the cursor over the path). This is useful "
"for aligning object horizontally or vertically, like with comic panels."
msgstr ""
"Permet ajustar amb una línia horitzontal o vertical des dels nodes dels "
"objectes vectorials existents (llevat que només es tracti de canviar la mida "
"de l'alçada o amplada, en aquest cas podeu arrossegar el cursor sobre el "
"camí). Això és útil per alinear els objectes en horitzontal o vertical, com "
"amb les vinyetes dels còmics."

#: ../../user_manual/snapping.rst:52
msgid "Orthogonal (Vector Only)"
msgstr "Ortogonal (només vectors)"

#: ../../user_manual/snapping.rst:55
msgid ".. image:: images/snapping/Snap-node.png"
msgstr ".. image:: images/snapping/Snap-node.png"

#: ../../user_manual/snapping.rst:57
msgid "Node (Vector Only)"
msgstr "Node (només vectors)"

#: ../../user_manual/snapping.rst:57
msgid "This snaps a vector node or an object to the nodes of another path."
msgstr "Ajusta un node o objecte vectorial amb els nodes d'un altre camí."

#: ../../user_manual/snapping.rst:60
msgid ".. image:: images/snapping/Snap-extension.png"
msgstr ".. image:: images/snapping/Snap-extension.png"

#: ../../user_manual/snapping.rst:62
msgid ""
"When we draw an open path, the last nodes on either side can be "
"mathematically extended. This option allows you to snap to that. The "
"direction of the node depends on its side handles in path editing mode."
msgstr ""
"Quan dibuixem un camí obert, els últims nodes de cada costat es podran "
"estendre matemàticament. Aquesta opció permet ajustar-ho. La direcció del "
"node dependrà de les seves nanses laterals en el mode edició de camins."

#: ../../user_manual/snapping.rst:65
msgid "Extension (Vector Only)"
msgstr "Extensió (només vectors)"

#: ../../user_manual/snapping.rst:68
msgid ".. image:: images/snapping/Snap-intersection.png"
msgstr ".. image:: images/snapping/Snap-intersection.png"

#: ../../user_manual/snapping.rst:69
msgid "Intersection (Vector Only)"
msgstr "Intersecció (només vectors)"

#: ../../user_manual/snapping.rst:70
msgid "This allows you to snap to an intersection of two vectors."
msgstr "Permet ajustar amb una intersecció de dos vectors."

#: ../../user_manual/snapping.rst:71
msgid "Bounding box (Vector Only)"
msgstr "Quadre contenidor (només vectors)"

#: ../../user_manual/snapping.rst:72
msgid "This allows you to snap to the bounding box of a vector shape."
msgstr "Permet ajustar amb el quadre contenidor d'una forma vectorial."

#: ../../user_manual/snapping.rst:74
msgid "Image bounds"
msgstr "Límits de la imatge"

#: ../../user_manual/snapping.rst:74
msgid "Allows you to snap to the vertical and horizontal borders of an image."
msgstr "Permet ajustar amb les vores verticals i horitzontals d'una imatge."

#: ../../user_manual/snapping.rst:77
msgid "Allows you to snap to the horizontal and vertical center of an image."
msgstr "Permet ajustar amb el centre vertical i horitzontal d'una imatge."

#: ../../user_manual/snapping.rst:78
msgid "Image center"
msgstr "Centre de la imatge"

#: ../../user_manual/snapping.rst:80
msgid "The snap works for the following tools:"
msgstr "L'ajustament funciona per a les següents eines:"

#: ../../user_manual/snapping.rst:82
msgid "Straight line"
msgstr "Línia recta"

#: ../../user_manual/snapping.rst:83
msgid "Rectangle"
msgstr "Rectangle"

#: ../../user_manual/snapping.rst:84
msgid "Ellipse"
msgstr "El·lipse"

#: ../../user_manual/snapping.rst:85
msgid "Polyline"
msgstr "Polilínia"

#: ../../user_manual/snapping.rst:86
msgid "Path"
msgstr "Camí"

#: ../../user_manual/snapping.rst:87
msgid "Freehand path"
msgstr "Camí a mà alçada"

#: ../../user_manual/snapping.rst:88
msgid "Polygon"
msgstr "Polígon"

#: ../../user_manual/snapping.rst:89
msgid "Gradient"
msgstr "Degradat"

#: ../../user_manual/snapping.rst:90
msgid "Shape Handling tool"
msgstr "Eina per al maneig de les formes"

#: ../../user_manual/snapping.rst:91
msgid "The Text-tool"
msgstr "L'eina de text"

#: ../../user_manual/snapping.rst:92
msgid "Assistant editing tools"
msgstr "Eines per als assistents d'edició"

#: ../../user_manual/snapping.rst:93
msgid ""
"The move tool (note that it snaps to the cursor position and not the "
"bounding box of the layer, selection or whatever you are trying to move)"
msgstr ""
"L'eina de moure (tingueu en compte que s'ajusta amb la posició del cursor i "
"no amb el quadre contenidor de la capa, selecció o el que intenteu moure)."

#: ../../user_manual/snapping.rst:96
msgid "The Transform tool"
msgstr "L'eina de transformació"

#: ../../user_manual/snapping.rst:97
msgid "Rectangle select"
msgstr "Selecció de rectangle"

#: ../../user_manual/snapping.rst:98
msgid "Elliptical select"
msgstr "Selecció el·líptica"

#: ../../user_manual/snapping.rst:99
msgid "Polygonal select"
msgstr "Selecció poligonal"

#: ../../user_manual/snapping.rst:100
msgid "Path select"
msgstr "Selecció del camí"

#: ../../user_manual/snapping.rst:101
msgid "Guides themselves can be snapped to grids and vectors"
msgstr "Les pròpies guies es poden ajustar amb les quadrícules i vectors"

#: ../../user_manual/snapping.rst:103
msgid ""
"Snapping doesn’t have a sensitivity yet, and by default is set to 10 screen "
"pixels."
msgstr ""
"L'ajust encara no té sensibilitat, de manera predeterminada està establert a "
"10 píxels de la pantalla."
