# Translation of docs_krita_org_general_concepts___projection.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_general_concepts___projection\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 09:09+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/projection.rst:None
msgid ".. image:: images/category_projection/projection-cube_09.svg"
msgstr ".. image:: images/category_projection/projection-cube_09.svg"

#: ../../general_concepts/projection.rst:1
msgid "The Perspective Projection Category."
msgstr "Категорія проєкції перспективи."

#: ../../general_concepts/projection.rst:15
msgid "Perspective Projection"
msgstr "Проєкція перспективи"

#: ../../general_concepts/projection.rst:17
msgid ""
"The Perspective Projection tutorial is one of the Kickstarter 2015 tutorial "
"rewards. It's about something that humanity has known scientifically for a "
"very long time, and decent formal training will teach you about this. But I "
"think there are very very few tutorials about it in regard to how to achieve "
"it in digital painting programs, let alone open source."
msgstr ""
"Підручник із проєкції перспективи отримав одну з нагород за підручники на "
"Kickstarter 2015. Його присвячено темі, яка відома людству з наукової точки "
"зору доволі тривалий час, темі, з якою можна ознайомитися під час "
"формального навчання. Втім, здається, існує дуже-дуже мало підручників, які "
"описують отримання відповідних результатів у програмах із цифрового "
"малювання, навіть якщо не обмежуватися програмами із відкритим кодом."

#: ../../general_concepts/projection.rst:19
msgid ""
"The tutorial is a bit image heavy, and technical, but I hope the skill it "
"teaches will be really useful to anyone trying to get a grasp on a "
"complicated pose. Enjoy, and don't forget to thank `Raghukamath <https://www."
"raghukamath.com/>`_ for choosing this topic!"
msgstr ""
"У цьому підручнику дещо забагато зображень, але, сподіваємося, навички, "
"якими ви оволодієте за його допомогою, є насправді корисними для будь-кого, "
"хто намагається вивчити це складне питання. Насолоджуйтеся і не забудьте "
"подякувати `Раджукамату <https://www.raghukamath.com/>`_ за те, що вибрав цю "
"тему!"

#: ../../general_concepts/projection.rst:24
msgid "Parts:"
msgstr "Частини:"
