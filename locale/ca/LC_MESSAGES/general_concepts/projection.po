# Translation of docs_krita_org_general_concepts___projection.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 19:37+0100\n"
"Last-Translator: Josep Ma. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/projection.rst:None
msgid ".. image:: images/category_projection/projection-cube_09.svg"
msgstr ".. image:: images/category_projection/projection-cube_09.svg"

#: ../../general_concepts/projection.rst:1
msgid "The Perspective Projection Category."
msgstr "La categoria Projecció en perspectiva."

#: ../../general_concepts/projection.rst:15
msgid "Perspective Projection"
msgstr "Projecció en perspectiva"

#: ../../general_concepts/projection.rst:17
msgid ""
"The Perspective Projection tutorial is one of the Kickstarter 2015 tutorial "
"rewards. It's about something that humanity has known scientifically for a "
"very long time, and decent formal training will teach you about this. But I "
"think there are very very few tutorials about it in regard to how to achieve "
"it in digital painting programs, let alone open source."
msgstr ""
"La guia d'aprenentatge Projecció en perspectiva és una de les recompenses de "
"la guia d'aprenentatge en el Kickstarter de 2015. Es tracta d'una cosa que "
"la humanitat ha sabut científicament durant molt de temps, i un entrenament "
"formal decent us l'ensenyarà. Però crec que hi ha molt poques guies "
"d'aprenentatge en referència a com aconseguir-ho en els programes de pintura "
"digital, i molt menys en el codi obert."

# skip-rule: t-acc_obe
#: ../../general_concepts/projection.rst:19
msgid ""
"The tutorial is a bit image heavy, and technical, but I hope the skill it "
"teaches will be really useful to anyone trying to get a grasp on a "
"complicated pose. Enjoy, and don't forget to thank `Raghukamath <https://www."
"raghukamath.com/>`_ for choosing this topic!"
msgstr ""
"La guia d'aprenentatge és rica en imatges i tècnica, però espero que "
"l'habilitat que ensenya sigui realment útil per a qualsevol que intenti "
"comprendre una postura complicada. Gaudiu-ne i no oblideu agrair a en "
"`Raghukamath <https://www.raghukamath.com/>`_ per triar aquest tema!"

#: ../../general_concepts/projection.rst:24
msgid "Parts:"
msgstr "Parts:"
