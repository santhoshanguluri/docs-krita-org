# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-05-05 18:10+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Configure Logging"
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:1
msgid "Overview of the log viewer docker."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:10
#: ../../reference_manual/dockers/log_viewer.rst:16
msgid "Log Viewer"
msgstr "Logweergave"

#: ../../reference_manual/dockers/log_viewer.rst:10
msgid "Debug"
msgstr "Debuggen"

#: ../../reference_manual/dockers/log_viewer.rst:18
msgid ""
"The log viewer docker allows you to see debug output without access to a "
"terminal. This is useful when trying to get a tablet log or to figure out if "
"Krita is spitting out errors while a certain thing is happening."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:20
msgid ""
"The log docker is used by pressing the :guilabel:`enable logging` button at "
"the bottom."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:24
msgid ""
"When enabling logging, this output will not show up in the terminal. If you "
"are missing debug output in the terminal, check that you didn't have the log "
"docker enabled."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:26
msgid ""
"The docker is composed of a log area which shows the debug output, and four "
"buttons at the bottom."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:29
msgid "Log Output Area"
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:31
msgid "The log output is formatted as follows:"
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:33
msgid "White"
msgstr "Wit"

#: ../../reference_manual/dockers/log_viewer.rst:34
msgid "This is just a regular debug message."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:35
msgid "Yellow"
msgstr "Geel"

#: ../../reference_manual/dockers/log_viewer.rst:36
msgid "This is a info output."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:37
msgid "Orange"
msgstr "Oranje"

#: ../../reference_manual/dockers/log_viewer.rst:38
msgid "This is a warning output."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:40
msgid "Red"
msgstr "Rood"

#: ../../reference_manual/dockers/log_viewer.rst:40
msgid "This is a critical error. When this is bolded, it is a fatal error."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:43
msgid "Options"
msgstr "Opties"

#: ../../reference_manual/dockers/log_viewer.rst:45
msgid "There's four buttons at the bottom:"
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:47
msgid "Enable Logging"
msgstr "Loggen inschakelen"

#: ../../reference_manual/dockers/log_viewer.rst:48
msgid "Enable the docker to start logging. This caries over between sessions."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:49
msgid "Clear the Log"
msgstr "De log wissen"

#: ../../reference_manual/dockers/log_viewer.rst:50
msgid "This empties the log output area."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:51
msgid "Save the Log"
msgstr "De log opslaan"

#: ../../reference_manual/dockers/log_viewer.rst:52
msgid "Save the log to a text file."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:54
msgid ""
"Configure which kind of debug is added. By default only warnings and simple "
"debug statements are logged. You can enable the special debug messages for "
"each area here."
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:56
msgid "General"
msgstr "Algemeen"

#: ../../reference_manual/dockers/log_viewer.rst:57
msgid "Resource Management"
msgstr "Hulpbronbeheer"

#: ../../reference_manual/dockers/log_viewer.rst:58
msgid "Image Core"
msgstr "Kern van afbeelding"

#: ../../reference_manual/dockers/log_viewer.rst:59
msgid "Registries"
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:60
msgid "Tools"
msgstr "Hulpmiddelen"

#: ../../reference_manual/dockers/log_viewer.rst:61
msgid "Tile Engine"
msgstr "Tegel-engine"

#: ../../reference_manual/dockers/log_viewer.rst:62
msgid "Filters"
msgstr "Filters"

#: ../../reference_manual/dockers/log_viewer.rst:63
msgid "Plugin Management"
msgstr "Plug-in-beheer"

#: ../../reference_manual/dockers/log_viewer.rst:64
msgid "User Interface"
msgstr "Gebruikersinterface"

#: ../../reference_manual/dockers/log_viewer.rst:65
msgid "File Loading and Saving"
msgstr "Laden en opslaan van bestand"

#: ../../reference_manual/dockers/log_viewer.rst:66
msgid "Mathematics and Calculations"
msgstr "Wiskunde en berekeningen"

#: ../../reference_manual/dockers/log_viewer.rst:67
msgid "Image Rendering"
msgstr "Renderen van afbeelding"

#: ../../reference_manual/dockers/log_viewer.rst:68
msgid "Scripting"
msgstr "Scripting"

#: ../../reference_manual/dockers/log_viewer.rst:69
msgid "Input Handling"
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:70
msgid "Actions"
msgstr "Acties"

#: ../../reference_manual/dockers/log_viewer.rst:71
msgid "Tablet Handing"
msgstr ""

#: ../../reference_manual/dockers/log_viewer.rst:72
msgid "GPU Canvas"
msgstr "Werkblad van GPU"

#: ../../reference_manual/dockers/log_viewer.rst:73
msgid "Metadata"
msgstr "Metagegevens"

#: ../../reference_manual/dockers/log_viewer.rst:74
msgid "Color Management"
msgstr "Kleurbeheer"
