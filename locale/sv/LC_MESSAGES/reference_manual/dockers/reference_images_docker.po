# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-30 20:59+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/dockers/reference_images_docker.rst:1
msgid "Overview of the pattern docker."
msgstr "Översikt av mönsterpanelen."

#: ../../reference_manual/dockers/reference_images_docker.rst:12
msgid "Reference"
msgstr "Referens"

#: ../../reference_manual/dockers/reference_images_docker.rst:17
msgid "Reference Images Docker"
msgstr "Referensbildpanelen"

#: ../../reference_manual/dockers/reference_images_docker.rst:21
msgid ""
"This docker was removed in Krita 4.0 due to crashes on Windows. :ref:`The "
"reference images tool in 4.1 replaces it. <reference_images_tool>`"
msgstr ""
"Panelen har tagits bort i Krita 4.0 på grund av krascher på Windows. :ref:"
"`Referensbildverktyget i 4.1 ersätter den. <reference_images_tool>`"

#: ../../reference_manual/dockers/reference_images_docker.rst:24
msgid ""
".. image:: images/dockers/400px-Krita_Reference_Images_Browse_Docker.png"
msgstr ""
".. image:: images/dockers/400px-Krita_Reference_Images_Browse_Docker.png"

#: ../../reference_manual/dockers/reference_images_docker.rst:26
msgid ".. image:: images/dockers/400px-Krita_Reference_Images_Image_Docker.png"
msgstr ""
".. image:: images/dockers/400px-Krita_Reference_Images_Image_Docker.png"

#: ../../reference_manual/dockers/reference_images_docker.rst:27
msgid ""
"This docker allows you to pick an image from outside of Krita and use it as "
"a reference. Even better, you can pick colors from it directly."
msgstr ""
"Panelen låter dig välja en bild utanför Krita och använda den som en "
"referens. Vad som är ännu bättre är att man kan direkt välja färger från den."

#: ../../reference_manual/dockers/reference_images_docker.rst:29
msgid "The docker consists of two tabs: Browsing and Image."
msgstr "Panelen består av två flikar: Bläddring och bild."

#: ../../reference_manual/dockers/reference_images_docker.rst:32
msgid "Browsing"
msgstr "Bläddring"

#: ../../reference_manual/dockers/reference_images_docker.rst:34
msgid ""
"Browsing gives you a small file browser, so you can navigate to the map "
"where the image you want to use as reference is located."
msgstr ""
"Bläddring ger en liten filbläddrare så att du kan navigera till katalogen "
"där bilden som man vill använda som referens finns."

#: ../../reference_manual/dockers/reference_images_docker.rst:36
msgid ""
"There's an image strip beneath the browser, allowing you to select the image "
"which you want to use. Double click to load it in the :guilabel:`Image` tab."
msgstr ""
"Det finns en bildrad nedanför bläddraren som gör det möjligt att välja "
"bilden som man vill använda. Dubbelklicka för att läsa in den under fliken :"
"guilabel:`Bild`."

#: ../../reference_manual/dockers/reference_images_docker.rst:39
msgid "Image"
msgstr "Bild"

#: ../../reference_manual/dockers/reference_images_docker.rst:41
msgid ""
"This tab allows you to see the images you selected, and change the zoom "
"level. Clicking anywhere on the image will allow you to pick the merged "
"color from it. Using the cross symbol, you can remove the icon."
msgstr ""
"Fliken låter dig se bilderna som valdes, och ändra zoomnivån. Genom att "
"klicka på bilden kan man välja den sammanfogade färgen från den. Genom att "
"använda kryssymbolen kan ikonen tas bort."
