# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:26+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita guilabel kbd advancedcolorselectordocker ref\n"

#: ../../<generated>:1
msgid "Make Brush Color Bluer"
msgstr "Tornar a Cor do Pincel Mais Azul"

#: ../../reference_manual/preferences/color_selector_settings.rst:1
msgid "The color selector settings in Krita."
msgstr "A configuração do selector de cores no Krita."

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Preferences"
msgstr "Preferências"

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Settings"
msgstr "Configuração"

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Color Selector"
msgstr "Selecção de Cores"

#: ../../reference_manual/preferences/color_selector_settings.rst:11
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/preferences/color_selector_settings.rst:16
msgid "Color Selector Settings"
msgstr "Configuração da Selecção de Cores"

#: ../../reference_manual/preferences/color_selector_settings.rst:18
msgid ""
"These settings directly affect Advanced Color Selector Dockers and the same "
"dialog box appears when the user clicks the settings button in that docker "
"as well. They also affect certain hotkey actions."
msgstr ""
"Estas definições afectam directamente as Áreas de Selecção Avançada de "
"Cores, aparecendo a mesma janela quando o utilizador carrega no botão de "
"configuração nessa área. Também afectam algumas acções com as teclas rápidas "
"ou de atalho."

#: ../../reference_manual/preferences/color_selector_settings.rst:20
msgid ""
"This settings menu has a drop-down for Advanced Color Selector, and Color "
"Hotkeys."
msgstr ""
"Este menu de configuração tem uma lista para o Selector Avançado de Cores e "
"para os Atalhos de Cores."

#: ../../reference_manual/preferences/color_selector_settings.rst:23
msgid "Advanced Color Selector"
msgstr "Selector de Cores Avançado"

#: ../../reference_manual/preferences/color_selector_settings.rst:25
msgid ""
"These settings are described on the page for the :ref:"
"`advanced_color_selector_docker`."
msgstr ""
"Estas opções estão descritas na página da :ref:"
"`advanced_color_selector_docker`."

#: ../../reference_manual/preferences/color_selector_settings.rst:28
msgid "Color Hotkeys"
msgstr "Teclas Rápidas de Cores"

#: ../../reference_manual/preferences/color_selector_settings.rst:30
msgid "These allow you to set the steps for the following actions:"
msgstr ""
"Estas teclas rápidas ou de atalho permitem-lhe definir os passos para as "
"seguintes acções:"

#: ../../reference_manual/preferences/color_selector_settings.rst:32
msgid "Make Brush Color Darker"
msgstr "Tornar a Cor do Pincel Mais Escura"

#: ../../reference_manual/preferences/color_selector_settings.rst:33
msgid ""
"This is defaultly set to :kbd:`K` key and uses the :guilabel:`lightness` "
"steps. This uses luminance when possible."
msgstr ""
"Por omissão, está configurada para a tecla :kbd:`K` e usa os passos da :"
"guilabel:`luminosidade`. Isto usa a luminosidade sempre que possível."

#: ../../reference_manual/preferences/color_selector_settings.rst:34
msgid "Make Brush Color Lighter"
msgstr "Tornar a Cor do Pincel Mais Clara"

#: ../../reference_manual/preferences/color_selector_settings.rst:35
msgid ""
"This is defaultly set to :kbd:`L` key and uses the :guilabel:`lightness` "
"steps. This uses luminance when possible."
msgstr ""
"Por omissão, está configurada para a tecla :kbd:`L` e usa os passos de :"
"guilabel:`luminosidade`. Isto usa a luminosidade sempre que possível."

#: ../../reference_manual/preferences/color_selector_settings.rst:36
msgid "Make Brush Color More Saturated"
msgstr "Tornar a Cor do Pincel Mais Saturada"

#: ../../reference_manual/preferences/color_selector_settings.rst:37
#: ../../reference_manual/preferences/color_selector_settings.rst:39
msgid "This is defaultly unset and uses the :guilabel:`saturation` steps."
msgstr ""
"Por omissão, não tem qualquer tecla atribuída e usa os passos da :guilabel:"
"`saturação`."

#: ../../reference_manual/preferences/color_selector_settings.rst:38
msgid "Make Brush Color More Desaturated"
msgstr "Tornar a Cor do Pincel Menos Saturada"

#: ../../reference_manual/preferences/color_selector_settings.rst:40
msgid "Shift Brushcolor Hue clockwise"
msgstr "Deslocar a Matiz da Cor do Pincel no Sentido Horário"

#: ../../reference_manual/preferences/color_selector_settings.rst:41
#: ../../reference_manual/preferences/color_selector_settings.rst:43
msgid "This is defaultly unset and uses the :guilabel:`Hue` steps."
msgstr ""
"Por omissão, não tem qualquer tecla atribuída e usa os passos da :guilabel:"
"`matiz`."

#: ../../reference_manual/preferences/color_selector_settings.rst:42
msgid "Shift Brushcolor Hue counter-clockwise"
msgstr "Deslocar a Matiz da Cor do Pincel no Sentido Anti-Horário"

#: ../../reference_manual/preferences/color_selector_settings.rst:44
msgid "Make Brush Color Redder"
msgstr "Tornar a Cor do Pincel Mais Vermelha"

#: ../../reference_manual/preferences/color_selector_settings.rst:45
#: ../../reference_manual/preferences/color_selector_settings.rst:47
msgid "This is defaultly unset and uses the :guilabel:`Redder/Greener` steps."
msgstr ""
"Por omissão, não tem qualquer tecla atribuída e usa os passos da :guilabel:"
"`Mais Vermelho/Verde`."

#: ../../reference_manual/preferences/color_selector_settings.rst:46
msgid "Make Brush Color Greener"
msgstr "Tornar a Cor do Pincel Mais Verde"

#: ../../reference_manual/preferences/color_selector_settings.rst:48
msgid "Make Brush Color Yellower"
msgstr "Tornar a Cor do Pincel Mais Amarela"

#: ../../reference_manual/preferences/color_selector_settings.rst:49
#: ../../reference_manual/preferences/color_selector_settings.rst:51
msgid "This is defaultly unset and uses the :guilabel:`Bluer/Yellower` steps."
msgstr ""
"Por omissão, não tem qualquer tecla atribuída e usa os passos da :guilabel:"
"`Mais Azul/Amarela`."
