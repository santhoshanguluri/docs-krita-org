# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:12+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en YouTube image scores menuselection inktilt\n"
"X-POFile-SpellExtra: assistanttool gpen Strokewrist images alt Hufflepuff\n"
"X-POFile-SpellExtra: Inkgpen inkbrush Strokearm Inkfillcircle gif\n"
"X-POFile-SpellExtra: Strokerigger Inkingpatterned ref Strokefingers\n"
"X-POFile-SpellExtra: Strokeshoulder Inkingaliasresize inking kbd Bézier\n"
"X-POFile-SpellExtra: Krita Inkconvex vs Backspace Revoy InkGpen\n"
"X-POFile-SpellExtra: pathselectiontool Inkspeed guilabel\n"

#: ../../tutorials/inking.rst:None
msgid ""
".. image:: images/inking/Stroke_fingers.gif\n"
"   :alt: finger movement"
msgstr ""
".. image:: images/inking/Stroke_fingers.gif\n"
"   :alt: movimento do dedo"

#: ../../tutorials/inking.rst:None
msgid ""
".. image:: images/inking/Stroke_wrist.gif\n"
"   :alt: wrist movement"
msgstr ""
".. image:: images/inking/Stroke_wrist.gif\n"
"   :alt: movimento do pulso"

#: ../../tutorials/inking.rst:None
msgid ""
".. image:: images/inking/Stroke_arm.gif\n"
"   :alt: arm movement"
msgstr ""
".. image:: images/inking/Stroke_arm.gif\n"
"   :alt: movimento do braço"

#: ../../tutorials/inking.rst:None
msgid ""
".. image:: images/inking/Stroke_shoulder.gif\n"
"   :alt: stroke shoulder movement"
msgstr ""
".. image:: images/inking/Stroke_shoulder.gif\n"
"   :alt: movimento do ombro no traço"

#: ../../tutorials/inking.rst:None
msgid ""
".. image:: images/inking/Inking_aliasresize.png\n"
"   :alt: aliased resize"
msgstr ""
".. image:: images/inking/Inking_aliasresize.png\n"
"   :alt: dimensionamento com contornos vincados"

#: ../../tutorials/inking.rst:1
msgid "tips and tricks for inking in Krita"
msgstr "sugestões e truques para pinturas no Krita"

#: ../../tutorials/inking.rst:13
msgid "Inking"
msgstr "Pinturas"

#: ../../tutorials/inking.rst:15
msgid ""
"The first thing to realize about inking is that unlike anatomy, perspective, "
"composition or color theory, you cannot compensate for lack of practice with "
"study or reasoning. This is because all the magic in drawing lines happens "
"from your shoulder to your fingers, very little of it happens in your head, "
"and your lines improve with practice."
msgstr ""
"A primeira coisa a pensar na pintura é que, ao contrário da anatomia, da "
"perspectiva, da composição ou da teoria da scores, não poderá compensar a "
"falta de prática com o estudo ou o raciocínio. É por isso que toda a magia "
"no desenho de linhas acontece do seu ombro até aos seus dedos, sendo que "
"muito pouco disto acontece na sua cabeça, sendo que as suas linhas melhoram "
"com a prática."

#: ../../tutorials/inking.rst:17
msgid ""
"On the other hand, this can be a blessing. You don’t need to worry about "
"whether you are smart enough, or are creative enough to be a good inker. "
"Just dedicated. Doubtlessly, inking is the Hufflepuff of drawing disciplines."
msgstr ""
"Por outro lado, pode ser uma vantagem. Não tem de se preocupar se é "
"inteligente ou criativo o suficiente para ser um bom pintor. Basta ser "
"dedicado. Sem dúvida, a pintura é o Hufflepuff das disciplinas de desenho."

#: ../../tutorials/inking.rst:19
msgid "That said, there are a few tips to make life easy:"
msgstr "Posto isto, existem algumas dicas para tornar isto simples:"

#: ../../tutorials/inking.rst:22
msgid "Pose"
msgstr "Pose"

#: ../../tutorials/inking.rst:24
msgid ""
"Notice how I mentioned up there that the magic happens between your "
"shoulders and fingers? A bit weird, not? But perhaps, you have heard of "
"people talking about adopting a different pose for drawing."
msgstr ""
"Lembra-se que mencionei acima que a magia acontece entre os seus ombros e os "
"dedos? É um pouco estranho, não? Mas talvez já tenha ouvido falar de pessoas "
"que dizem que adoptam uma pose diferente para desenhar."

#: ../../tutorials/inking.rst:26
msgid ""
"You can in fact, make different strokes depending on which muscles and "
"joints you use to make the movement: The Fingers, the wrist and lower-arm "
"muscles, the elbow and upper-arm muscles or the shoulder and back muscles."
msgstr ""
"Poderá, de facto, fazer diferentes traços dependendo de quais os músculos ou "
"articulações que usa para criar o movimento: os dedos, o pulso e os músculos "
"do antebraço, o cotovelo, os músculos do braço ou os músculos das costas e "
"dos ombros."

#: ../../tutorials/inking.rst:34
msgid ""
"Generally, the lower down the arm the easier it is to make precise strokes, "
"but also the less durable the joints are for long term use. We tend to start "
"off using our fingers and wrist a lot during drawing, because it’s easier to "
"be precise this way. But it’s difficult to make long strokes, and "
"furthermore, your fingers and wrist get tired far quicker."
msgstr ""
"De um modo geral, quanto mais baixo for no braço, mais fácil será desenhar "
"traços com precisão, mas também menos duração terão as articulações em uso a "
"longo prazo. Tendemos a começar a desenhar com os nossos dedos e pulso na "
"maior parte do desenho, porque é mais fácil ter precisão desta forma. Mas "
"torna-se difícil fazer traços longos e, para além disso, os seus dedos e o "
"pulso ficarão cansados muito mais depressa."

#: ../../tutorials/inking.rst:42
msgid ""
"Your shoulders and elbows on the other hand are actually quite good at "
"handling stress, and if you use your whole hand you will be able to make "
"long strokes far more easily. People who do calligraphy need shoulder based "
"strokes to make those lovely flourishes (personally, I can recommend "
"improving your handwriting as a way to improve inking), and train their arms "
"so they can do both big and small strokes with the full arm."
msgstr ""
"Os seus ombros e cotovelos, por outro lado, são bastante bons a lidar com o "
"'stress' e, se usar a sua mão por inteiro, conseguirá fazer traços longos "
"com muito mais facilidade. As pessoas que fazem caligrafia precisam de "
"traços baseados no ombro para criar aqueles floreados bonitos (pessoalmente, "
"é recomendado que melhore a sua escrita à mão como forma de melhorar a "
"pintura) e treinar os seus braços para que possam fazer tanto traços longos "
"como curtos com o braço inteiro."

#: ../../tutorials/inking.rst:44
msgid ""
"To control pressure in this state effectively, you should press your pinky "
"against the tablet surface as you make your stroke. This will allow you to "
"precisely judge how far the pen is removed from the tablet surface while "
"leaving the position up to your shoulders. The pressure should then be put "
"by your elbow."
msgstr ""
"Para controlar de forma eficaz a pressão neste estado, deverá pressionar o "
"seu polegar contra a superfície da tablete, à medida que vai fazendo o seu "
"traço. Isto permitir-lhe-á julgar com precisão quão longe está a caneta da "
"superfície da tablete, enquanto deixa a posição na responsabilidade dos seus "
"ombros. A pressão deverá então ser aplicada pelo seu cotovelo."

#: ../../tutorials/inking.rst:46
msgid ""
"So, there are not any secret rules to inking, but if there is one, it would "
"be the following: *The longer your stroke, the more of your arms you need to "
"use to make the stroke*."
msgstr ""
"Como tal, não existe nenhum segredo em relação à pintura, mas se existisse, "
"seria o seguinte: *Quanto mais longe pinta, mais será necessário dos seus "
"braços para fazer o traço*."

#: ../../tutorials/inking.rst:49
msgid "Stroke smoothing"
msgstr "Suavização do traço"

#: ../../tutorials/inking.rst:51
msgid ""
"So, if the above is the secret to drawing long strokes, that would be why "
"people having been inking lovely drawings for years without any smoothing? "
"Then, surely, it is decadence to use something like stroke smoothing, a "
"short-cut for the lazy?"
msgstr ""
"Por isso, se o que está acima é o segredo para desenhar traços longos, "
"deveria ser por isso que as pessoas conseguiam pintar desenhos bonitos "
"durante anos sem qualquer suavização? Então, deverá ser de certeza decadente "
"usar algo como a suavização, um atalho para os preguiçosos?"

#: ../../tutorials/inking.rst:56
msgid ""
".. image:: images/inking/Stroke_rigger.gif\n"
"   :alt: rigger brush demonstration"
msgstr ""
".. image:: images/inking/Stroke_rigger.gif\n"
"   :alt: demonstração do pincel fino"

#: ../../tutorials/inking.rst:56
msgid ""
"Example of how a rigger brush can smooth the original movement (here in red)"
msgstr ""
"Exemplo de como um pincel fino poderá suavizar o movimento original (aqui a "
"vermelho)"

#: ../../tutorials/inking.rst:58
msgid ""
"Not really. To both, actually. Inkers have had a real-life tool that made it "
"easier to ink, it’s called a rigger-brush, which is a brush with very long "
"hairs. Due to this length it sorta smooths out shakiness, and thus a "
"favoured brush when inking at three in the morning."
msgstr ""
"Nem por isso. Para ambos, de facto. Os pintores têm tido uma ferramenta na "
"vida real que lhes facilita muito a pintura, que é o pincel fino; este é um "
"pincel com pêlos muito compridos. Devido ao seu tamanho, suaviza de certa "
"forma as tremuras, e é por isso um pincel favorecido quando pinta às três da "
"manhã."

#: ../../tutorials/inking.rst:60
msgid ""
"With some tablet brands, the position events being sent aren’t very precise, "
"which is why we having basic smoothing to apply the tiniest bit of smoothing "
"on tablet strokes."
msgstr ""
"Com algumas marcas de tabletes, os eventos de posição que são enviados não "
"são muito precisos, e é por isso que temos alguma suavização básica para "
"aplicar no mínimo sobre os traços da tablete."

#: ../../tutorials/inking.rst:62
msgid ""
"On the other hand, doing too much smoothing during the whole drawing can "
"make your strokes very mechanical in the worst way. Having no jitter or tiny "
"bumps removes certain humanity from your drawings, and it can make it "
"impossible to represent fabric properly."
msgstr ""
"Por outro lado, fazer demasiadas suavizações durante todo o desenho poderá "
"tornar os seus traços demasiado mecânicos da pior forma. Sem ter qualquer "
"agitação ou interrupção, retira uma certa humanidade do seu desenho e pode "
"impossibilitar a representação do tecido de forma adequada."

#: ../../tutorials/inking.rst:64
msgid ""
"Therefore, it’s wise to train your inking hand, yet not to be too hard on "
"yourself and refuse to use smoothing at all, as we all get tired, cold or "
"have a bad day once in a while. Stabilizer set to 50 or so should provide a "
"little comfort while keeping the little irregularities."
msgstr ""
"Como tal, é sábio treinar a sua mão de pintura; contudo, não seja demasiado "
"duro consigo próprio, recusando-se a usar qualquer suavização, já que todos "
"ficamos cansados, constipados ou temos um dia mau de vez em quando. Com o "
"estabilizador configurado a 50 ou algo aproximado, deverá obter algum "
"pequeno conforto enquanto mantém as pequenas irregularidades."

#: ../../tutorials/inking.rst:67
msgid "Bezier curves and other tools"
msgstr "Curvas Bézier e outras ferramentas"

#: ../../tutorials/inking.rst:69
msgid ""
"So, you may have heard of a French curve. If not, it’s a piece of plastic "
"representing a stencil. These curves are used to make perfectly smooth "
"curves on the basis of a sketch."
msgstr ""
"Bem, poderá já ter ouvido falar de uma curva Francesa. Caso contrário, é uma "
"peça de plástico que representa um molde. Essas curvas são usadas para criar "
"curvas perfeitamente suaves na base de um rascunho."

#: ../../tutorials/inking.rst:71
msgid ""
"In digital painting, we don’t have the luxury of being able to use two "
"hands, so you can’t hold a ruler with one hand and adjust it while inking "
"with the other. For this purpose, we have instead Bezier curves, which can "
"be made with the :ref:`path_selection_tool`."
msgstr ""
"Na pintura digital, não temos o luxo de conseguir usar duas mãos, pelo que "
"não conseguirá manter uma régua com uma mão e ajustá-la enquanto pinta com a "
"outra. Para esse fim, temos por outro lado as curvas de Bézier, que podem "
"ser criadas com a :ref:`path_selection_tool`."

#: ../../tutorials/inking.rst:73
msgid ""
"You can even make these on a vector layer, so they can be modified on the "
"fly."
msgstr ""
"Poderá até usá-las sobre uma camada vectorial, de modo que possam ser "
"modificadas na hora."

#: ../../tutorials/inking.rst:75
msgid ""
"The downside of these is that they cannot have line-variation, making them a "
"bit robotic."
msgstr ""
"A desvantagem destas é que não poderão ter variações das linhas, o que as "
"torna um pouco robóticas."

#: ../../tutorials/inking.rst:77
msgid ""
"You can also make small bezier curves with the :ref:`assistant_tool`, "
"amongst the other tools there."
msgstr ""
"Também poderá criar pequenas curvas Bézier com a :ref:`assistant_tool`, "
"entre outras ferramentas aqui."

#: ../../tutorials/inking.rst:79
msgid ""
"Then, in the freehand brush tool options, you can tick :guilabel:`Snap to "
"Assistants` and start a line that snaps to this assistant."
msgstr ""
"Depois, nas opções da ferramenta do pincel à mão, poderá assinalar a opção :"
"guilabel:`Ajustar aos Assistentes` e iniciar uma linha que se ajusta a este "
"assistente."

#: ../../tutorials/inking.rst:82
msgid "Presets"
msgstr "Predefinições"

#: ../../tutorials/inking.rst:84
msgid ""
"So here are some things to consider with the brush-presets that you use:"
msgstr ""
"Como tal, existem algumas coisas a ter em consideração com as predefinições "
"de pincéis que pode usar:"

#: ../../tutorials/inking.rst:87
msgid "Anti-aliasing versus jagged pixels"
msgstr "Suavização vs pixels tremidos"

#: ../../tutorials/inking.rst:89
msgid ""
"A starting inker might be inclined to always want to use anti-aliased "
"brushes, after all, they look so smooth on the screen. However, while these "
"look good on screen, they might become fuzzy when printing them. Therefore, "
"Krita comes with two default types. Anti-aliased brushes like ink_brush_25 "
"and slightly aliased brushes like ink_tilt, with the latter giving better "
"print results. If you are trying to prepare for both, it might be an idea to "
"consider making the inking page 600dpi and the color page 300dpi, so that "
"the inking page has a higher resolution and the ‘jaggies’ aren’t as visible. "
"You can turn any pixel brush into an aliased brush, by going the :kbd:`F5` "
"key and ticking **Sharpness**."
msgstr ""
"Um pintor iniciante poderá estar inclinado a usar sempre pincéis suavizados; "
"afinal de contas, parecem tão suaves no seu ecrã. Contudo, embora possam "
"parecer bonitos o ecrã, poderão ficar estranhos quando os imprimir. Como "
"tal, o Krita vem com dois tipos por omissão. Os pincéis suavizados, como o "
"'ink_brush_25' e os pincéis pouco suavizados, como o 'ink_tilt', sendo que o "
"último gera melhores resultados na impressão. Se está a tentar preparar-se "
"para ambos, poderá ser uma ideia criar a página da pintura a 600ppp e a "
"página da cor a 300ppp, pelo que a página de pintura tem uma maior resolução "
"e as ‘tremuras’ não são tão visíveis. Poderá transformar qualquer pincel de "
"pixels num pincel sem suavização, se for a :kbd:`F5` e assinalar o "
"**Afiamento** (Nitidez)."

#: ../../tutorials/inking.rst:92
msgid "Texture"
msgstr "Textura"

#: ../../tutorials/inking.rst:94
msgid ""
"Do you make smooth ‘wet’ strokes? Or do you make textured ones? For the "
"longest time, smooth strokes were preferred, as that would be less of a "
"headache when entering the coloring phase. Within Krita there are several "
"methods to color these easily, the colorize mask being the prime example, so "
"textured becomes a viable option even for the lazy amongst us."
msgstr ""
"Costuma fazer traços suaves e  ‘húmidos’? Ou cria traços com textura? "
"Durante a maior parte do tempo, os traços suaves eram preferidos, já que "
"seria menos uma dor de cabeça quando entrasse na fase de colori-los. Dentro "
"do Krita, existem vários métodos para os colorir com facilidade, sendo a "
"máscara de coloração o exemplo principal; assim, as texturas tornaram-se uma "
"opção viável, mesmo para os preguiçosos entre nós."

#: ../../tutorials/inking.rst:99
msgid ""
".. image:: images/inking/Inking_patterned.png\n"
"   :alt: type of strokes"
msgstr ""
".. image:: images/inking/Inking_patterned.png\n"
"   :alt: tipos de traços"

#: ../../tutorials/inking.rst:99
msgid "Left: No texture, Center: Textured, Right: Predefined Brush tip"
msgstr ""
"Esquerda: Sem textura, Centro: Com textura, Direita: Ponta do pincel "
"predefinida"

#: ../../tutorials/inking.rst:102
msgid "Pressure curve"
msgstr "Curva de pressão"

#: ../../tutorials/inking.rst:104
msgid ""
"Of course, the nicest lines are made with pressure sensitivity, so they "
"dynamically change from thick to thin. However, different types of curves on "
"the pressure give different results. The typical example is a slightly "
"concave line to create a brush that more easily makes thin lines."
msgstr ""
"Obviamente, as linhas mais bonitas são feitas com sensibilidade à pressão, "
"pelo que vão variando de forma dinâmica de grossas para finas. Contudo, os "
"diferentes tipos de curvas na pressão geram diferentes resultados. O exemplo "
"típico seria uma linha ligeiramente côncava para criar um pincel que possa "
"criar mais facilmente as linhas finas."

#: ../../tutorials/inking.rst:109
msgid ""
".. image:: images/inking/Ink_gpen.png\n"
"   :alt: pressure curve for ink gpen"
msgstr ""
".. image:: images/inking/Ink_gpen.png\n"
"   :alt: curva de pressão para a 'gpen' de pintura"

#: ../../tutorials/inking.rst:109
msgid ""
"Ink_Gpen_25 is a good example of a brush with a concave pressure curve. This "
"curve makes it easier to make thin lines."
msgstr ""
"A Ink_Gpen_25 é um bom exemplo de um pincel com uma curva de pressão "
"côncava. Esta curva ajuda a criar linhas finas."

#: ../../tutorials/inking.rst:114
msgid ""
".. image:: images/inking/Ink_convex.png\n"
"   :alt: convex inking brush"
msgstr ""
".. image:: images/inking/Ink_convex.png\n"
"   :alt: pincel de pintura convexo"

#: ../../tutorials/inking.rst:114
msgid "conversely, here's a convex brush. The strokes are much rounder"
msgstr ""
"por outro lado, aqui está um pincel convexo. Os traços são muito mais "
"redondos"

#: ../../tutorials/inking.rst:119
msgid ""
".. image:: images/inking/Ink_fill_circle.png\n"
"   :alt: ink fill circle"
msgstr ""
".. image:: images/inking/Ink_fill_circle.png\n"
"   :alt: círculo de preenchimento da tinta"

#: ../../tutorials/inking.rst:119
msgid ""
"Fill_circle combines both into an s-curve, this allows for very dynamic "
"brush strokes"
msgstr ""
"O círculo preenchido combina ambos numa curva-S, que permite traços do "
"pincel muito dinâmicos"

#: ../../tutorials/inking.rst:124
msgid ""
".. image:: images/inking/Ink_speed.png\n"
"   :alt: inverse convex to speed parameter"
msgstr ""
".. image:: images/inking/Ink_speed.png\n"
"   :alt: parâmetro inverso de convexo para velocidade"

#: ../../tutorials/inking.rst:124
msgid ""
"Pressure isn't the only thing you can do interesting things with, adding an "
"inverse convex curve to speed can add a nice touch to your strokes"
msgstr ""
"A pressão não é a única coisa que pode usar para criar coisas interessantes; "
"se adicionar uma curva convexa inversa para a velocidade, poderá obter um "
"toque engraçado aos seus traços"

#: ../../tutorials/inking.rst:127
msgid "Preparing sketches for inking"
msgstr "Preparar os rascunhos para a pintura"

#: ../../tutorials/inking.rst:129
msgid ""
"So, you have a sketch and you wish to start inking it. Assuming you’ve "
"scanned it in, or drew it, you can try the following things to make it "
"easier to ink."
msgstr ""
"Como tal, tem um rascunho e quer começar a pintá-lo. Assumindo que já o "
"digitalizou ou o desenhou, poderá tentar as seguintes coisas para facilitar "
"a pintura."

#: ../../tutorials/inking.rst:132
msgid "Opacity down to 10%"
msgstr "Reduza a opacidade a 10%"

#: ../../tutorials/inking.rst:134
msgid ""
"Put a white (just press the :kbd:`Backspace` key) layer underneath the "
"sketch. Turn down the opacity of the sketch to a really low number and put a "
"layer above it for inking."
msgstr ""
"Coloque uma camada branca (basta carregar em :kbd:`Backspace`) por baixo do "
"rascunho. Reduza a opacidade do rascunho para um valor realmente baixo e "
"coloque uma camada sobre ele para a pintura."

#: ../../tutorials/inking.rst:137
msgid "Make the sketch colored"
msgstr "Torne o rascunho colorido"

#: ../../tutorials/inking.rst:139
msgid ""
"Put a layer filled with a color you like between the inking and sketch "
"layer. Then set that layer to ‘screen’ or ‘addition’, this will turn all the "
"black lines into the color! If you have a transparent background, or put "
"this layer into a group, be sure to tick the alpha-inherit symbol!"
msgstr ""
"Coloque uma camada preenchida com uma dada cor que goste entre a camada de "
"pintura e do rascunho. Depois configure essa camada como ‘ecrã’ ou ‘adição’; "
"isto irá transformar todas as linhas pretas na cor! Se tiver um fundo "
"transparente, ou colocar esta camada sobre um grupo, certifique-se que "
"assinala o símbolo de herança do alfa!"

#: ../../tutorials/inking.rst:142
msgid "Make the sketch colored, alternative version"
msgstr "Tornar o rascunho colorido, versão alternativa"

#: ../../tutorials/inking.rst:144
msgid ""
"Or, right-click the layer, go to layer properties, and untick ‘blue’. This "
"works easier with a single layer sketch, while the above works best with "
"multi-layer sketches."
msgstr ""
"Ou então, carregue com o botão direito sobre a camada, vá às propriedades da "
"camada e desligue o 'azul'. Isto funciona melhor num rascunho com uma única "
"camada, enquanto o anterior funciona melhor em rascunhos com várias camadas."

#: ../../tutorials/inking.rst:147
msgid "Super-thin lines"
msgstr "Linhas super-finas"

#: ../../tutorials/inking.rst:149
msgid ""
"If you are interested in super-thin lines, it might be better to make your "
"ink at double or even triple the size you usually work at, and, only use an "
"aliased pixel brush. Then, when the ink is finished, use the fill tool to "
"fill in flats on a separate layer, split the layer via :menuselection:`Layer "
"--> Split --> Layer Split`, and then resize to the original size."
msgstr ""
"Se estiver interessado nas linhas super-finas, poderá ser melhor fazer a sua "
"pintura com o dobro ou o triplo do tamanho com que costuma trabalhar e "
"usando só um pincel de pixels sem suavização. Depois, quando terminar a "
"pintura, use a ferramenta de preenchimento para pintar de forma plana numa "
"camada em separado; divida a camada com a opção :menuselection:`Camada --> "
"Dividir --> Dividir a Camada`, e depois volte a dimensionar para o tamanho "
"original."

#: ../../tutorials/inking.rst:154
msgid ""
"This might be a little of an odd way of working, but it does make drawing "
"thin lines trivial, and it's cheaper to buy RAM so you can make HUGE images "
"than to spent hours on trying to color the thin lines precisely, especially "
"as colorize mask will not be able to deal with thin anti-aliased lines very "
"well."
msgstr ""
"Isto poderá ser uma forma um pouco estranha de trabalhar, mas facilita "
"bastante o desenho de linhas finas, e é mais barato comprar RAM, pelo que "
"poderá criar imagens ENORMES em vez de perder horas a colorir com precisão "
"as linhas finas, especialmente por que a máscara de coloração não será capaz "
"de lidar muito bem com as linhas finas com suavização."

#: ../../tutorials/inking.rst:157
msgid ""
"David Revoy made a set of his own inking tips for Krita and explains them in "
"this `youtube video <https://www.youtube.com/watch?v=xvQ5l0edsq4>`_."
msgstr ""
"O David Revoy criou um conjunto com as suas próprias dicas de pintura para o "
"Krita e explica-as neste `vídeo do YouTube <https://www.youtube.com/watch?"
"v=xvQ5l0edsq4>`_."
