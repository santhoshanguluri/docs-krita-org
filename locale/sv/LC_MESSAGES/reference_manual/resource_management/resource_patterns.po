# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-06 20:22+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/resource_management/resource_patterns.rst:None
msgid ".. image:: images/patterns/Krita_Patterns.png"
msgstr ".. image:: images/patterns/Krita_Patterns.png"

#: ../../reference_manual/resource_management/resource_patterns.rst:None
msgid ".. image:: images/patterns/Generating_custom_patterns1.png"
msgstr ".. image:: images/patterns/Generating_custom_patterns1.png"

#: ../../reference_manual/resource_management/resource_patterns.rst:None
msgid ".. image:: images/patterns/Generating_custom_patterns2.png"
msgstr ".. image:: images/patterns/Generating_custom_patterns2.png"

#: ../../reference_manual/resource_management/resource_patterns.rst:None
msgid ".. image:: images/patterns/Generating_custom_patterns3.png"
msgstr ".. image:: images/patterns/Generating_custom_patterns3.png"

#: ../../reference_manual/resource_management/resource_patterns.rst:1
msgid "Creating and managing patterns in Krita."
msgstr "Skapa och hantera mönster i Krita."

#: ../../reference_manual/resource_management/resource_patterns.rst:11
#: ../../reference_manual/resource_management/resource_patterns.rst:16
msgid "Patterns"
msgstr "Mönster"

#: ../../reference_manual/resource_management/resource_patterns.rst:11
msgid "Resources"
msgstr "Resurser"

#: ../../reference_manual/resource_management/resource_patterns.rst:21
msgid ""
"Patterns are small raster files that tile. They can be used as following:"
msgstr ""
"Mönster är små rasterfiler som läggs ut i rutor. De kan användas på följande "
"sätt:"

#: ../../reference_manual/resource_management/resource_patterns.rst:23
msgid "As fill for a vector shape."
msgstr "För att fylla i en vektorform."

#: ../../reference_manual/resource_management/resource_patterns.rst:24
msgid "As fill-tool color."
msgstr "Som färg för fyllverktyget."

#: ../../reference_manual/resource_management/resource_patterns.rst:25
msgid "As height-map for a brush using the 'texture' functionality."
msgstr ""
"Som höjdavbildning för en pensel genom att använda 'strukturfunktionen'."

#: ../../reference_manual/resource_management/resource_patterns.rst:26
msgid "As fill for a generated layer."
msgstr "För att fylla i ett genererat lager."

#: ../../reference_manual/resource_management/resource_patterns.rst:29
msgid "Adding new patterns"
msgstr "Lägga till nya mönster"

#: ../../reference_manual/resource_management/resource_patterns.rst:31
msgid ""
"You can add new patterns via the pattern docker, or the pattern-quick-access "
"menu in the toolbar. At the bottom of the docker, beneath the resource-"
"filter input field, there are the :guilabel:`Import resource` and :guilabel:"
"`Delete resource` buttons. Select the former to add png or jpg files to the "
"pattern list."
msgstr ""
"Man kan lägga till nya mönster via mönsterpanelen, eller menyn för "
"snabbåtkomst av mönster i verktygsraden. Längst ner i panelen, under "
"resursfiltrets inmatningsfält, finns knapparna :guilabel:`Importera resurs` "
"och :guilabel:`Ta bort resurs`. Använd den föregående för att lägga till "
"png- eller jpg-filer i mönsterlistan."

#: ../../reference_manual/resource_management/resource_patterns.rst:34
msgid ""
"Similarly, removing patterns can be done by pressing the :guilabel::`Delete "
"resource` button. Krita will not delete the actual file then, but rather "
"black list it, and thus not load it."
msgstr ""
"På liknande sätt kan mönster tas bort genom att klicka på knappen :guilabel::"
"`Ta bort resurs`. Krita tar då inte bort själva filen, utan svartlistar den "
"istället, och läser därigenom inte in den."

#: ../../reference_manual/resource_management/resource_patterns.rst:37
msgid "Temporary patterns and generating patterns from the canvas"
msgstr "Tillfälliga mönster och generera mönster från duken"

#: ../../reference_manual/resource_management/resource_patterns.rst:39
msgid ""
"You can use the pattern drop-down to generate patterns from the canvas but "
"also to make temporary ones."
msgstr ""
"Mönsterkombinationsrutan kan användas för att generera mönster från duken "
"men också skapa tillfälliga."

#: ../../reference_manual/resource_management/resource_patterns.rst:41
msgid "First, draw a pattern and open the pattern drop-down."
msgstr "RIta först ett mönster och öppna mönsterkombinationsrutan."

#: ../../reference_manual/resource_management/resource_patterns.rst:46
msgid ""
"Then go into :guilabel:`custom` and first press :guilabel:`Update` to show "
"the pattern in the docker. Check if it's right. Here, you can also choose "
"whether you use this layer only, or the whole image. Since 3.0.2, Krita will "
"take into account the active selection as well when getting the information "
"of the two."
msgstr ""
"Gå därefter till :guilabel:`egen` och klicka först på :guilabel:`Uppdatera` "
"för att visa mönstret i panelen. Kontrollera att det är riktigt. Här går det "
"också att välja om bara det här lagret används, eller hela bilden. Sedan "
"3.0.2 tar Krita också hänsyn till den aktiva markeringen när information om "
"båda hämtas."

#: ../../reference_manual/resource_management/resource_patterns.rst:51
msgid ""
"Then, click either :guilabel:`Use as Pattern` to use it as a temporary "
"pattern, or :guilabel:`Add to predefined patterns` to save it into your "
"pattern resources!"
msgstr ""
"Klicka sedan antingen på :guilabel:`Använd som mönster` för att använda det "
"som ett tillfälligt mönster, eller :guilabel:`Lägg till i fördefinierade "
"mönster` för att spara det i mönsterresurserna."

#: ../../reference_manual/resource_management/resource_patterns.rst:53
msgid ""
"You can then start using it in Krita by for example making a canvas and "
"doing :guilabel:`Edit --> Fill with Pattern`."
msgstr ""
"Man kan därefter börja använda det i Krita genom att exempelvis skapa en duk "
"och använda :guilabel:`Redigera --> Fyll med mönster`."

#: ../../reference_manual/resource_management/resource_patterns.rst:59
msgid ":ref:`pattern_docker`"
msgstr ":ref:`pattern_docker`"

#: ../../reference_manual/resource_management/resource_patterns.rst:61
msgid "You can tag patterns here, and filter them with the resource filter."
msgstr "Man kan etikettera mönster här, och filtrera dem med resursfiltret."
